import React from "react"
import * as S from "./styles"
import { graphql, useStaticQuery } from "gatsby"
import BackgroundImage from "gatsby-background-image"
import { Container, Row, Col } from "react-grid-system"

const Login = () => {
  const data = useStaticQuery(graphql`
    query {
      background: file(relativePath: { eq: "background.png" }) {
        childImageSharp {
          fluid(maxWidth: 300) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `)
  return (
    <S.Container>
      <S.StyledBackgroundImage fluid={data.background.childImageSharp.fluid}>
        <Container fluid>
          <Row>
            <Col style={{ height: "100vh" }}>test</Col>
          </Row>
        </Container>
      </S.StyledBackgroundImage>
    </S.Container>
  )
}

export default Login
