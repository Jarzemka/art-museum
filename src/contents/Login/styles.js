import styled from "styled-components"
import BackgroundImage from "gatsby-background-image"

export const Container = styled.div``

export const StyledBackgroundImage = styled(BackgroundImage)`
  background-size: contain;
`
