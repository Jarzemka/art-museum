import React from "react"
import { StaticQuery, graphql } from "gatsby"
import { SEO } from "../components"
import Login from "../contents/Login"
import GlobalTemplate from "../theme/GlobalTemplate"
import styled from "styled-components"

const Wrapper = styled.div`
  position: relative;
`

const IndexPage = () => (
  <GlobalTemplate>
    <StaticQuery
      query={graphql`
        query IndexPageQuery {
          site {
            siteMetadata {
              title
            }
          }
        }
      `}
      render={data => (
        <Wrapper>
          <SEO title={data.site.siteMetadata.title} />
          <Login />
        </Wrapper>
      )}
    />
  </GlobalTemplate>
)

export default IndexPage
