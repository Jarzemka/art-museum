import { createGlobalStyle } from "styled-components"

const url = "https://fonts.googleapis.com/css?family=Montserrat&display=swap"
const GlobalStyle = createGlobalStyle`

*, *::before, *::after {
   font-family: ${({ theme }) => theme.font.family};
    box-sizing: border-box;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

  
  html {
    font-size: 16px;
  }
  
  body {
    margin: 0;
  }

  h1, h2, h3, h4, h5, h6 {
    margin: 16px 0;
  }

  p, span {
    margin: 0;
  }

  a {
    text-decoration: none;
  }
`

export default GlobalStyle
