import React from "react"
import { ThemeProvider } from "styled-components"
import GlobalStyles from "./GlobalStyles"
import theme from "./theme"


const GlobalTemplate = ({ children, active }) => (
  <ThemeProvider theme={theme}>
    <div>
      <GlobalStyles />
      <main>{children}</main>
    </div>
  </ThemeProvider>
)

export default GlobalTemplate
