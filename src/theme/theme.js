const theme = {
  color: {
    red: "#FF473A",
    gray: "#979797",
  },
  font: {
    family: "Montserrat",
  },
}

export default theme
